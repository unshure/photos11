package fields.application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class Album implements Serializable{

	private String name;
	
	private ArrayList<Photo> Photos;
	
	/**
	 * default constructor, initalized name to null and makes an empty array list of photos
	 */
	public Album() {
		this.name =null;
		Photos = new ArrayList<Photo>();
	}
	/**
	 * Initalizes name and makes an empty array list of photos
	 * @param name Name of Album
	 */
	public Album(String name) {
		this.name = name;
		Photos = new ArrayList<Photo>();
	}
	/**
	 * initalizes name and photos
	 * @param name Name of Album
	 * @param Photos Array List of photos
	 */
	public Album(String name, ArrayList<Photo> Photos) {
		this.name = name;
		this.Photos = Photos; 
	}
	/**
	 * returns Arraylist of Photos
	 * @return ArrayList of photos
	 */
	public ArrayList<Photo> getPhotos(){
		return Photos;
	}
	/**
	 * Sets current list of photos
	 * @param Photos New set of Photos
	 */
	public void setPhotos(ArrayList<Photo> Photos) {
		this.Photos = Photos;
	}
	/**
	 * Adds a photo to the list of photos within the album. Does not add if the photo already exists
	 * @param photo New photo
	 */
	public void addPhoto(Photo photo) {
		if(!Photos.contains(photo)) {
			Photos.add(photo);
		}
	}
	/**
	 * Removes the photo from within the album
	 * @param photo Photo to remove
	 */
	public void removePhoto(Photo photo) {
		Photos.remove(Photos.indexOf(photo));
	}
	/**
	 * returns name of album
	 * @return name of album
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets the name of the album
	 * @param name New name for Album
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Prints name of Album
	 */
	public String toString() {
		return name;
	}
	public String getLowDate() {
		if(this.Photos.size() == 0) {
			return "";
		}
		Date lowdate = this.Photos.get(0).getDate().getTime();
		for(Photo p:this.Photos) {
			if(p.getDate().getTime().before(lowdate)) {
				lowdate = p.getDate().getTime();
			}
		}
		return lowdate.toString();
	}
	public String getHighDate() {
		if(this.Photos.size() == 0) {
			return "";
		}
		Date lowdate = this.Photos.get(0).getDate().getTime();
		for(Photo p:this.Photos) {
			if(p.getDate().getTime().after(lowdate)) {
				lowdate = p.getDate().getTime();
			}
		}
		return lowdate.toString();
	}
}
