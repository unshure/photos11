package fields.application;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class User implements Serializable{
	
	private String username;
	private String password;
	
	public ArrayList<Album> Albums;
	public ArrayList<Photo> photoGallery;
	/**
	 * Default Constructor. Initalizes everything to null
	 */
	public User() {
		this.username = null;
		this.password = null;
		Albums = new ArrayList<Album>();
	}
	/**
	 * Initalized Username and Password
	 * @param name Username
	 * @param pass Password
	 */
	public User(String name, String pass) {
		this.username = name;
		this.password = pass;
		Albums = new ArrayList<Album>();
	}
	/**
	 * returns Username
	 * @return Username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * Sets a new Username
	 * @param username New Username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * Returns the Password
	 * @return Password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Sets a new password
	 * @param password New Password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Returns the Arraylist of Ablums stored by the user
	 * @return Album Array List
	 */
	public ArrayList<Album> getAlbums(){
		return Albums;
	}

}
