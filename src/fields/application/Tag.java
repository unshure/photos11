package fields.application;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class Tag implements Serializable{

	private String name;
	private String value;
	/**
	 * Initalize tag with a name and a vale
	 * @param name Name
	 * @param value Value
	 */
	public Tag(String name, String value) {
		this.name = name;
		this.value = value;
	}
	/**
	 * Returns name of tag
	 * @return Name of tag
	 */
	public String getName() {
		return name;
	}
	/**
	 * returns Value of tag
	 * @return Value of tag
	 */
	public String getValue() {
		return value;
	}
	/**
	 * Prints following info of tag: name:value
	 */
	public String toString() {
		return name + ":" + value;
	}
	/**
	 * Static Method to parse text that is going to be a list of tags
	 * @param s String of format "name1:value1; name2:value2; ..."
	 * @return ArrayList of Tags
	 */
	public static ArrayList<Tag> parseTags(String s) {
		if(s.length() == 0) {
			return new ArrayList<Tag>();
		}
		ArrayList<Tag> tags = new ArrayList<Tag>();
		String[] tagArray = s.split(";");
		for(String tag: tagArray) {
			tag = tag.trim();
			String[] keyvalpair = tag.split(":");
			if(keyvalpair.length != 2) {
				return null;
			}
			tags.add(new Tag(keyvalpair[0].toLowerCase().trim(),keyvalpair[1].toLowerCase().trim()));
		}
				
		return tags;
	}

}
