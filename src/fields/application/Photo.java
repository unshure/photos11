package fields.application;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class Photo implements Serializable{

	public String caption;
	public ArrayList<Tag> Tags;
	public String address;
	public Calendar lastModified;
	/**
	 * Initalized Caption and file location of photo. Initalized the Last Modified Date
	 * @param caption Caption
	 * @param address File Location
	 */
	public Photo(String caption, String address) {
		this.caption = caption;
		this.address = address;
		Tags = new ArrayList<Tag>();
		File file = new File(address);
		lastModified= Calendar.getInstance();
		Date date = new Date(file.lastModified());
		lastModified.setTime(date);
		lastModified.set(Calendar.MILLISECOND, 0);
	}
	/**
	 * Initalized Caption, file location, and tags for the photo. Initalized the Last Modified Date
	 * @param caption
	 * @param address
	 * @param Tags
	 */
	public Photo(String caption, String address, ArrayList<Tag> Tags) {
		this.caption = caption;
		this.address = address;
		this.Tags = Tags;
		
		File file = new File(address);
		lastModified= Calendar.getInstance();
		Date date = new Date(file.lastModified());
		lastModified.setTime(date);
		lastModified.set(Calendar.MILLISECOND, 0);
	}
	/**
	 * Adds new tags not already on the picture
	 * @param tags Arraylist of tags
	 */
	public void addTags(ArrayList<Tag> tags) {
		for(Tag tag:tags) {
			if(!Tags.contains(tag)) {
				Tags.add(tag);
			}
		}
	}
	/**
	 * Returns date of the Photo
	 * @return Photo date
	 */
	public Calendar getDate() {
		return lastModified;
	}
	/**
	 * Removes tag from photo
	 * @param tag Tag to remove
	 */
	public void removeTag(Tag tag) {
		Tags.remove(Tags.indexOf(tag));
	}
	/**
	 * Returns list of tags attached to photo
	 * @return Array List of Tags
	 */
	public ArrayList<Tag> getTags() {
		return Tags;
	}
	/**
	 * Returns File Locaiton of photo
	 * @return File Locaiton
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * Returns Caption for Photo
	 * @return Caption
	 */
	public String getCaption() {
		return caption;
	}
	/**
	 * Sets new caption for photo
	 * @param caption New Caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public boolean hasTag(Tag tag) {
		for(Tag t: Tags) {
			if (tag.getValue().equals(t.getValue()) && tag.getName().equals(t.getName())){
				return true;
			}
		}
		return false;
	}
	public String toString() {
		return caption;
	}
	
    // Overriding equals() to compare two Complex objects
    @Override
    public boolean equals(Object o) {
 
        // If the object is compared with itself then return true  
        if (o == this) {
            return true;
        }
 
        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Photo)) {
            return false;
        }
         
        // typecast o to Complex so that we can compare data members 
        Photo p = (Photo) o;
         
        // Compare the data members and return accordingly 
        if(this.getAddress().equals(p.getAddress()) && this.getCaption().equals(p.getCaption())) {
        	return true;
        }
        return false;
    }
	
}
