package application;
	
import java.io.IOException;
import java.util.ArrayList;

import fields.application.Album;
import fields.application.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import view.application.*;

/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */

public class Photos extends Application {
	
	public static ArrayList<User> Users;
	
	/**
	 * The start method is what loads the first screen of the application. Loads the Login page
	 * @param primaryStage Main Stage for application
	 */
	@Override	
	public void start(Stage primaryStage) {
		
		try {
			Users = Closing.readApp();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/application/Login.fxml"));
			Parent root = loader.load();
			
			LoginController lc = loader.getController();
			lc.start(primaryStage);
			
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * Called when the application is stopped at any point. Used to serialize data between sessions
	 */
	@Override
	public void stop() throws IOException{
	    System.out.println("Stage is closing");
	    // Save file
	    Closing.writeApp(Users);
	}
	/**
	 * Main Method
	 * @param args Main Method arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}