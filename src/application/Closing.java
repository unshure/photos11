package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import fields.application.User;

/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class Closing {

	public static final String storeDir = "dat";
	public static final String storeFile = "users.dat";
	
	/**
	 * Serialized data and saved to dat/users.dat
	 * @param Users Arraylist of all of the users to be saved
	 * @throws IOException
	 */
	public static void writeApp(ArrayList<User> Users) throws IOException{
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(storeDir + File.separator + storeFile));
		oos.writeObject(Users);
	}
	
	/**
	 * Reads previously Serialized data
	 * @return ArrayList of all previous users and their data
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static ArrayList<User> readApp() throws IOException, ClassNotFoundException {
		try {
		ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(storeDir + File.separator + storeFile));
		ArrayList<User> Users = (ArrayList<User>)ois.readObject();
			return Users;
		}
		catch(Exception e){
			return new ArrayList<User>();
		}
	}
	
}
