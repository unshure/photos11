package view.application;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import fields.application.*;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class PhotoViewController {

	//List for songs and artists
	
	public User user;
	public Album album;
	public Photo photo;
	public int albumIndex;
	
	@FXML Button reCaption;
	@FXML Button addTag;
	@FXML Button deleteTag;
	@FXML Button previous;
	@FXML Button next;
	@FXML Button close;
	
	@FXML ListView<Tag> tagList;
	@FXML ImageView picture;

	
	@FXML Label photoCaption;
	
	private ObservableList<Tag> obsList;              

	
	Stage mainStage;
	/**
	 * Start method for Photo View. Sets the Picture, tags, name, date, and initalizes buttons for the PhotoView Scene
	 * @param mainStage Main Stage
	 * @param user Current User
	 * @param album Current Album
	 * @param photo Current Photo
	 * @param albumIndex Photos index within album. Used for slideshow.
	 */
	public void start(Stage mainStage,User user, Album album, Photo photo,int albumIndex) {
		this.mainStage = mainStage;

		this.user = user;
		this.album = album;
		this.photo = photo;
		this.albumIndex = albumIndex;
		photoCaption.setText("Caption: " + photo.getCaption() +"\nDate of Photo: " + photo.getDate().getTime().toString());
		
		FileInputStream input = null;
		Image image = null;
			try {
				input = new FileInputStream(photo.getAddress());
				image = new Image(input);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		picture.setImage(image);
		picture.fitHeightProperty();
		picture.fitWidthProperty();
		
		obsList = FXCollections.observableArrayList(photo.getTags());
		tagList.setItems(obsList);
		
		reCaption.setOnAction(this::reCaptionAction);
		addTag.setOnAction(this::addTagAction);
		deleteTag.setOnAction(this::deleteTagAction);
		next.setOnAction(this::nextAction);
		previous.setOnAction(this::previousAction);
		close.setOnAction(this::closeAction);
		
	}
	/**
	 * Changes the caption of the current photo and updates the text. New caption is inputted in popup window
	 * @param e
	 */
	public void reCaptionAction(ActionEvent e) {
		TextInputDialog dialog = new TextInputDialog(album.getName());
		dialog.setTitle("Re Caption");
		dialog.setHeaderText("Create a new Album Name");
		dialog.setContentText("Please enter the Album Name:");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			photo.setCaption(result.get());;
		}
		photoCaption.setText("Caption: " + photo.getCaption() + "\nDate of Photo: " + photo.getDate().getTime().toString());
	}
	/**
	 * Adds new tags to the current photo. A text prompt appears where the user can input many new tags for the photo
	 * @param e
	 */
	public void addTagAction(ActionEvent e) {
		TextInputDialog dialog = new TextInputDialog("NewTags");
		dialog.setTitle("NewTags");
		dialog.setHeaderText("Enter in new tags");
		dialog.setContentText("Tags (name1:value1; name2:value2; ...): \")");
		Optional<String> result = dialog.showAndWait();

		// The Java 8 way to get the response value (with lambda expression).
		result.ifPresent(tagString -> photo.addTags(Tag.parseTags(tagString)));
		obsList = FXCollections.observableArrayList(photo.getTags());
		tagList.setItems(obsList);
	}
	/**
	 * Deletes the currently selected tag in the tag ListView
	 * @param e
	 */
	public void deleteTagAction(ActionEvent e) {
		int index = tagList.getSelectionModel().getSelectedIndex();
		photo.removeTag(obsList.get(index));
		obsList = FXCollections.observableArrayList(photo.getTags());
		tagList.setItems(obsList);
		

	}
	/**
	 * Slideshow event to move onto the next photo in the album
	 * @param e
	 */
	public void nextAction(ActionEvent e) {			
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/application/PhotoView.fxml"));
			Parent root = loader.load();
			
			PhotoViewController pvc = loader.getController();
			albumIndex++;
			Photo newPhoto = album.getPhotos().get(albumIndex % album.getPhotos().size());
			pvc.start(mainStage,user,album,newPhoto,albumIndex);
			
			Scene scene = new Scene(root);
			mainStage.setScene(scene);
			mainStage.show();
		} catch (IOException ee) {
			// TODO Auto-generated catch block
			ee.printStackTrace();
		}
	}
	/**
	 * Slideshow event to move onto the previous photo in the album
	 * @param e
	 */
	public void previousAction(ActionEvent e) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/application/PhotoView.fxml"));
			Parent root = loader.load();
			
			
			PhotoViewController pvc = loader.getController();
			albumIndex--;
			Photo newPhoto = album.getPhotos().get(albumIndex % album.getPhotos().size());
			pvc.start(mainStage,user,album,newPhoto,albumIndex);
			
			Scene scene = new Scene(root);
			mainStage.setScene(scene);
			mainStage.show();
		} catch (IOException ee) {
			// TODO Auto-generated catch block
			ee.printStackTrace();
		}
	}
	/**
	 * Closes the current view of the photo and returns to the AlbumView
	 * @param e
	 */
	public void closeAction(ActionEvent e) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/application/AlbumView.fxml"));
			Parent root = loader.load();
			
			AlbumViewController abc = loader.getController();
			abc.start(mainStage,user,album);
			
			Scene scene = new Scene(root);
			mainStage.setScene(scene);
			mainStage.show();
		} catch (IOException ee) {
			// TODO Auto-generated catch block
			ee.printStackTrace();
		}
	}
}
