package view.application;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;

import fields.application.*;

/**
 * @author Nick Clegg
 * @author Kevin Wu
 */

public class UserSubSystemController {
	
	//List for songs and artists
		@FXML TilePane albumDisplay;
		
		@FXML Button addAlbum;
		@FXML Button deleteAlbum;
		@FXML Button editAlbum;
		@FXML Button logout;
		@FXML Button quit;
		@FXML Button searchButton;
		@FXML Button addSearchAlbum;
		
		@FXML TextField search;
		
		@FXML Label userInfo;
				
		public User user;
		public VBox selectedBox;
		
		Stage mainStage;
		
		public ArrayList<VBox> albumBoxList;
		public ArrayList<Album> albumList;
		
		public ArrayList<Photo> searchedPhotos = null;
		
		/**
		 * Start method for the UserSubSystem. Displays all Albums of the current user, shows the first picture in the album if the album contains any pictures, or a defualt picture if non exist yet.
		 * Also initalizes all buttons and defines the quit button with a lambda function.
		 * @param mainStage Main Stage
		 * @param user Current User
		 */
		public void start(Stage mainStage,User user) {
			this.mainStage = mainStage;
			this.user = user;
			
			userInfo.setText("Username: " + user.getUsername());

			
			selectedBox = null;
			
			albumList = user.getAlbums();
			
			albumBoxList = new ArrayList<VBox>();
				if(!albumList.isEmpty()) {
				for(Album album: albumList){
	
					albumBoxList.add(makeAlbumView(album));
				}
				//System.out.println(albumBoxList);
	
				this.updateAlbumView();
			}
				
			addSearchAlbum.setOnAction(this::addSearchAlbumAction);
			searchButton.setOnAction(this::searchButtonAction);
			editAlbum.setOnAction(this::editAlbumAction);
			addAlbum.setOnAction(this::addAlbumAction);
			deleteAlbum.setOnAction(this::deleteAlbumAction);
			logout.setOnAction(this::logoutAction);

			quit.setOnAction((event) -> {
				Platform.exit();
			});

		}
		
		/**
		 * adds search functionality to search bar. Includes either a tag search or a date search
		 * @param e
		 */
		public void searchButtonAction(ActionEvent e) {
			String toParse = search.getText();
			selectedBox = null;
			if(toParse.length() == 0) {
				updateAlbumView();
				searchedPhotos = null;
				return;
			}
			ArrayList<Photo> matches = new ArrayList<Photo>();
			ArrayList<Integer> op= new ArrayList<Integer>();
			ArrayList<Tag> tagTest = new ArrayList<Tag>();
			
			String[] parsed = toParse.split(" ");
			parsed[0] = parsed[0].toLowerCase();
			if(parsed[0].equals("date")) {
				if(parsed.length !=4) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("Improper Search Format");
					alert.setContentText("Search Formats are as follows:\n name1:value1 operator name2:value2 operator ...\nOperators are of the form (and,or,AND,OR,&&,||)\nAlso a date range search:\nDATE month/date/year to month/date/year");
					alert.showAndWait();
					return;
				}
				String[] firstDateString = parsed[1].split("/");
				String[] secondDateString = parsed[3].split("/");
				Integer year1,year2,month1,month2,day1,day2;
				try {
				year1 = Integer.parseInt(firstDateString[2]) - 1900;
				month1 = Integer.parseInt(firstDateString[0]) - 1;
				day1 = Integer.parseInt(firstDateString[1]);
				year2 = Integer.parseInt(secondDateString[2]) - 1900;
				month2 = Integer.parseInt(secondDateString[0]);
				day2 = Integer.parseInt(secondDateString[1]) - 1;
				}
				catch(Exception ee) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("Improper Search Format");
					alert.setContentText("Search Formats are as follows:\n name1:value1 operator name2:value2 operator ...\nOperators are of the form (and,or,AND,OR,&&,||)\nAlso a date range search:\nDATE month/date/year to month/date/year");
					alert.showAndWait();
					return;
				}
				Date firstDate = new Date(year1,month1,day1);
				Date secondDate = new Date(year2,month2,day2);
				
				//System.out.println(firstDate.toString());
				//System.out.println(secondDate.toString());

				
				ArrayList<Photo> photos = new ArrayList<Photo>();
				for(Album a: user.getAlbums()) {
					photos.addAll(a.getPhotos());
				}
				for(Photo photo: photos) {
					//System.out.println(photo.getDate().getTime().toString());
					if(firstDate.before(photo.getDate().getTime()) && secondDate.after(photo.getDate().getTime())) {
						if(!matches.contains(photo)) {
							matches.add(photo);
						}
					}
				}
				updatePhotoView(matches);
				searchedPhotos = matches;
				return;

			}
			for(String s: parsed) {
				s = s.toLowerCase();
				if(s.contains(":")) {
					String[] keyvalpair = s.split(":");
					if(keyvalpair.length != 2) {
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Warning");
						alert.setHeaderText("Improper Search Format");
						alert.setContentText("Search Formats are as follows:\n name1:value1 operator name2:value2 operator ...\nOperators are of the form (and,or,AND,OR,&&,||)\nAlso a date range search:\nDATE month/date/year to month/date/year");
						alert.showAndWait();
						return;
					}
					tagTest.add( new Tag(keyvalpair[0].toLowerCase().trim(),keyvalpair[1].toLowerCase().trim()));
				}else if(s.equals("and") || s.equals("or") || s.equals("&&") || s.equals("||")) {
					if(s.equals("and") || s.equals("&&")) {
						op.add(1);
					}else {
						op.add(0);
					}
				}else {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("Improper Search Format");
					alert.setContentText("Search Formats are as follows:\n\"name1:value1 operator name2:value2 operator ...\"\nOperators are of the form (and,or,AND,OR,&&,||)\nAlso a date range search:\n\"DATE month/date/year\"");
					alert.showAndWait();
					return;
				}
			}
			if(tagTest.size() ==0) {
				return;
			}
			ArrayList<Photo> photos = new ArrayList<Photo>();
			for(Album a: user.getAlbums()) {
				photos.addAll(a.getPhotos());
			}
			for(Photo photo: photos) {
				boolean result = photo.hasTag(tagTest.get(0));
				if(tagTest.size()>1) {
					for(int i = 0; i<op.size(); i++) {
						if(op.get(i) == 0) {
							result = result || photo.hasTag(tagTest.get(i+1));
						}else {
							result = result && photo.hasTag(tagTest.get(i+1));
						}
					}
				}
				if(result) {
					if(!matches.contains(photo)) {
						matches.add(photo);
					}				}
			}
			updatePhotoView(matches);
			searchedPhotos = matches;
		}
		/**
		 * Puts any pictures visible after a search into a new album
		 * @param e
		 */
		public void addSearchAlbumAction(ActionEvent e) {
			if(searchedPhotos != null) {
				TextInputDialog dialog = new TextInputDialog("Album Name");
				dialog.setTitle("New Album");
				dialog.setHeaderText("Create a new Album");
				dialog.setContentText("Please enter the Album name:");
				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()){
					for(Album a:user.getAlbums()) {
						if(a.getName().equals(result.get())){
							Alert alert = new Alert(AlertType.WARNING);
							alert.setTitle("Warning");
							alert.setHeaderText("Two Albums Cannot have the same name");
							alert.setContentText("Choose a new Album name");
							alert.showAndWait();
							addSearchAlbumAction(e);
							return;
						}
					}
					Album temp = new Album(result.get(),searchedPhotos);
					albumList.add(temp);
					albumBoxList.add(makeAlbumView(temp));
					updateAlbumView();
					searchedPhotos = null;
				}
			}
		}
		
		/**
		 * Action for the edit button. Switches to the Album view of the currently selectedBox album
		 * @param e Current action taking place
		 */
		public void editAlbumAction(ActionEvent e) {
			if(!(selectedBox == null)) {
				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/view/application/AlbumView.fxml"));
					Parent root = loader.load();
					
					AlbumViewController abc = loader.getController();
					abc.start(mainStage,user,albumList.get(albumBoxList.indexOf(selectedBox)));
					
					Scene scene = new Scene(root);
					mainStage.setScene(scene);
					mainStage.show();
				} catch (IOException ee) {
					// TODO Auto-generated catch block
					ee.printStackTrace();
				}
			}
		}
		/**
		 * Opens a prompt to create a new album. Prompt asks for the name of the new Album
		 * @param e
		 */
		public void addAlbumAction(ActionEvent e) {
			TextInputDialog dialog = new TextInputDialog("Album Name");
			dialog.setTitle("New Album");
			dialog.setHeaderText("Create a new Album");
			dialog.setContentText("Please enter the Album name:");
			Optional<String> result = dialog.showAndWait();
			if (result.isPresent()){
				for(Album a:user.getAlbums()) {
					if(a.getName().equals(result.get())){
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Warning");
						alert.setHeaderText("Two Albums Cannot have the same name");
						alert.setContentText("Choose a new Album name");
						alert.showAndWait();
						addAlbumAction(e);
						return;
					}
				}
				Album temp = new Album(result.get());
				albumList.add(temp);
				albumBoxList.add(makeAlbumView(temp));
				updateAlbumView();
			}
		}
		/**
		 * Deletes the currently selected album
		 * @param e
		 */
		public void deleteAlbumAction(ActionEvent e) {
			if(!(selectedBox == null)) {
				int i = albumBoxList.indexOf(selectedBox);
				albumBoxList.remove(i);
				albumList.remove(i);
				selectedBox = null;
				updateAlbumView();
			}
			
		}
		/**
		 * Logs out the current user and returns to the login Page
		 * @param e
		 */
		public void logoutAction(ActionEvent e) {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/application/Login.fxml"));
				Parent root = loader.load();
				
				LoginController lc = loader.getController();
				lc.start(mainStage);
				
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			} catch (IOException ee) {
				// TODO Auto-generated catch block
				ee.printStackTrace();
			}
		}
		/**
		 * Updates the view of the current albums for the user if they are added or deleted
		 */
		public void updateAlbumView() {
			albumDisplay.getChildren().clear();
			for(VBox album : albumBoxList) {
				if(album.equals(selectedBox)) {
					album.setStyle("-fx-padding: 0;" + 
		                      "-fx-border-style: solid inside;" + 
		                      "-fx-border-width: 2;" +
		                      "-fx-border-insets: 5;" + 
		                      "-fx-border-color: gray;");
				}else {
					album.setStyle("");
				}
				albumDisplay.getChildren().add(album);
			}
		}
		
		/**
		 * Makes a single album view icon
		 * @param album Album to be turned into an icon
		 * @return VBox object with picture and name
		 */
		public VBox makeAlbumView(Album album) {
			VBox view = new VBox();
			view.setAlignment(Pos.CENTER);

			FileInputStream input = null;
			Image image = null;
			if(album.getPhotos().isEmpty()) {
				

				image = new Image("/view/application/default.jpg");
			}else {
				try {
					input = new FileInputStream(album.getPhotos().get(0).address);
					image = new Image(input);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			ImageView imageView = new ImageView(image);
			imageView.setFitHeight(50);
			imageView.setFitWidth(50);

			Text title = new Text(album.getName());
			
			view.getChildren().add(imageView);
			view.getChildren().add(title);
			view.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					selectedBox = (VBox) event.getSource();
					updateAlbumView();
					event.consume();
				}
			});
			return view;
		}

		
		/**
		 * Displays currently searched photos
		 * @param Photos List of photos to display
		 */
		public void updatePhotoView(ArrayList<Photo> Photos){
			albumDisplay.getChildren().clear();
			for(Photo p:Photos) {
				albumDisplay.getChildren().add(makePhotoView(p));
			}
		}
		/**
		 * Creates a Vertical Box which houses the photo as an ImageView and the Images caption below that for displaying in the scene
		 * @param photo
		 * @return
		 */
		public VBox makePhotoView(Photo photo) {
			VBox view = new VBox();
			view.setAlignment(Pos.CENTER);
			
			FileInputStream input = null;
			Image image = null;
				try {
					input = new FileInputStream(photo.getAddress());
					image = new Image(input);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			ImageView imageView = new ImageView(image);
			imageView.setFitHeight(50);
			imageView.setFitWidth(50);

			Text title = new Text(photo.getCaption());
			
			view.getChildren().add(imageView);
			view.getChildren().add(title);

			return view;
		}
		
}
