package view.application;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import fields.application.*;

/**
 * @author Nick Clegg
 * @author Kevin Wu
 */
public class AlbumViewController {

	public User user;
	public Album album;
	
	@FXML TilePane photoDisplay;
	
	@FXML Button addPhoto;
	@FXML Button deletePhoto;
	@FXML Button editPhoto;
	@FXML Button movePhoto;
	@FXML Button copyPhoto;
	@FXML Button editAlbum;
	@FXML Button closeAlbum;

	
	
	@FXML Label AlbumInfo;
	
	private VBox selectedBox = null;
	public ArrayList<Photo> photoList;
	public ArrayList<VBox> photoBoxList;
	
	Stage mainStage;
	
	/**
	 * Start method for AlbumView. Displays all photos within the Album with their caption, as well as initialized all of the buttons within the scene
	 * @param mainStage
	 * @param user
	 * @param album
	 */
	public void start(Stage mainStage,User user,Album album) {
		this.mainStage = mainStage;
		this.user = user;
		this.album = album;
		
		updateText();

		photoList = album.getPhotos();
		photoBoxList = new ArrayList<VBox>();
		if(!photoList.isEmpty()) {
			for(Photo photo: photoList){
				photoBoxList.add(makePhotoView(photo));
			}
			updatePhotoView();
		}
		
		addPhoto.setOnAction(this::addPhotoAction);
		deletePhoto.setOnAction(this::deletePhotoAction);
		editPhoto.setOnAction(this::editPhotoAction);
		movePhoto.setOnAction(this::movePhotoAction);
		copyPhoto.setOnAction(this::copyPhotoAction);
		editAlbum.setOnAction(this::editAlbumAction);
		closeAlbum.setOnAction(this::closeAlbumAction);
		
		/* adds local pictures to the album*/
		/*
		Photo temp = new Photo("Squirtle", "Stock Photos/Squirtle.png");
		photoList.add(temp);
		photoBoxList.add(makePhotoView(temp));
		temp = new Photo("Bulbasaur", "Stock Photos/Bulbasaur.png");
		photoList.add(temp);
		photoBoxList.add(makePhotoView(temp));
		temp = new Photo("Charmander", "Stock Photos/Charmander.png");
		photoList.add(temp);
		photoBoxList.add(makePhotoView(temp));
		temp = new Photo("Mankey", "Stock Photos/Mankey.png");
		photoList.add(temp);
		photoBoxList.add(makePhotoView(temp));
		temp = new Photo("Growlithe", "Stock Photos/Growlithe.png");
		photoList.add(temp);
		photoBoxList.add(makePhotoView(temp));
		updatePhotoView();
		*/
		 
	}
		
	/**
	 * Add Photo action. Opens a prompt to look for a new photo, then asks for a name and list of tags for the new photo
	 * @param e
	 */
	public void addPhotoAction(ActionEvent e) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Find a Picture");
		File file = fileChooser.showOpenDialog(mainStage);
		FileInputStream input = null;
		if(file != null) {
			try {
				input = new FileInputStream(file.getAbsolutePath());
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return;
			}
			
			Dialog<Pair<String,String>> dialog = new Dialog<>();
			dialog.setTitle("New Photo");
			dialog.setHeaderText("Create a new Photo");
			dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(20, 150, 10, 10));

			TextField caption = new TextField();
			caption.setPromptText("Caption");
			TextField tags = new TextField();
			tags.setPromptText("Tags");

			grid.add(new Label("Caption: "), 0, 0);
			grid.add(caption, 1, 0);
			grid.add(new Label("Tags (name1:value1; name2:value2; ...): "), 0, 1);
			grid.add(tags, 1, 1);
			
			dialog.getDialogPane().setContent(grid);
			
			dialog.setResultConverter(dialogButton -> {
			    if (dialogButton == ButtonType.OK) {
			        return new Pair<>(caption.getText(), tags.getText());
			    }
			    return null;
			});
			
			Optional<Pair<String,String>> result = dialog.showAndWait();
			
			if (result.isPresent()){
				ArrayList<Tag> tagList = Tag.parseTags(result.get().getValue());
				if(tagList == null) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Improper Format");
					alert.setHeaderText("Improper tag format");
					alert.setContentText("You can enter tags in the format:\nname1:value1; name2:value2; ...");
					alert.showAndWait();
					return;
				}
				Photo temp = new Photo(result.get().getKey(), file.getAbsolutePath(),tagList);
				photoList.add(temp);
				photoBoxList.add(makePhotoView(temp));
				updatePhotoView();
			}
		}
		updateText();

	}
	/**
	 * Delete action to delete the currently selected photo
	 * @param e
	 */
	public void deletePhotoAction(ActionEvent e) {
		if(!(selectedBox == null)) {
			int i = photoBoxList.indexOf(selectedBox);
			photoBoxList.remove(i);
			photoList.remove(i);
			selectedBox = null;
			updatePhotoView();
		}
		updateText();

	}
	/**
	 * Edit action to open up the PhotoView for the currently selected photo
	 * @param e
	 */
	public void editPhotoAction(ActionEvent e) {
		if(!(selectedBox == null)) {
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/application/PhotoView.fxml"));
				Parent root = loader.load();
				
				int albumIndex = photoBoxList.indexOf(selectedBox);
				
				PhotoViewController pvc = loader.getController();
				pvc.start(mainStage,user,album,photoList.get(photoBoxList.indexOf(selectedBox)),albumIndex);
				
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			} catch (IOException ee) {
				// TODO Auto-generated catch block
				ee.printStackTrace();
			}
		}
	}
	/**
	 * Moves the currently selected photo to another album owned by the current user
	 * @param e
	 */
	public void movePhotoAction(ActionEvent e) {
		if(selectedBox != null) {
			// Create the custom dialog.
			Dialog<Album> dialog = new Dialog<>();
			dialog.setTitle("Move Dialog");
			dialog.setHeaderText("Move the photo");
	
			// Set the button types.
			dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
	
			// Create the username and password labels and fields.
			ListView<Album> list = new ListView<Album>();
	
			ObservableList<Album> obsList = FXCollections.observableArrayList(user.getAlbums());          
			list.setItems(obsList);
	
			dialog.getDialogPane().setContent(list);
	
			// Convert the result to a username-password-pair when the login button is clicked.
			dialog.setResultConverter(dialogButton -> {
			    if (dialogButton == ButtonType.OK) {
			    	int index = list.getSelectionModel().getSelectedIndex();
			        return obsList.get(index);
			    }
			    return null;
			});
	
			Optional<Album> result = dialog.showAndWait();
	
			result.ifPresent(newAlbum -> {
			    if(this.album.equals(newAlbum)) {
			    	Alert alert = new Alert(AlertType.ERROR);
			    	alert.setTitle("Error Dialog");
			    	alert.setHeaderText("You cannot choose the same Album");
			    	alert.setContentText("Choose a different Album");
			    	
			    	alert.showAndWait();
			    	copyPhotoAction(e);
			    	return;
			    }
			    newAlbum.addPhoto(photoList.get(photoBoxList.indexOf(selectedBox)));
			    deletePhotoAction(e);
			});
			
		}
	}
	/**
	 * Copies the currently selected photo to another album owned by the current user	
	 * @param e
	 */
	public void copyPhotoAction(ActionEvent e) {
		if(selectedBox != null) {
			// Create the custom dialog.
			Dialog<Album> dialog = new Dialog<>();
			dialog.setTitle("Copy Dialog");
			dialog.setHeaderText("Copy the photo");
	
			// Set the button types.
			dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
	
			// Create the username and password labels and fields.
			ListView<Album> list = new ListView<Album>();
	
			ObservableList<Album> obsList = FXCollections.observableArrayList(user.getAlbums());          
			list.setItems(obsList);
	
			dialog.getDialogPane().setContent(list);
	
			// Convert the result to a username-password-pair when the login button is clicked.
			dialog.setResultConverter(dialogButton -> {
			    if (dialogButton == ButtonType.OK) {
			    	int index = list.getSelectionModel().getSelectedIndex();
			        return obsList.get(index);
			    }
			    return null;
			});
	
			Optional<Album> result = dialog.showAndWait();
	
			result.ifPresent(newAlbum -> {
			    if(this.album.equals(newAlbum)) {
			    	Alert alert = new Alert(AlertType.ERROR);
			    	alert.setTitle("Error Dialog");
			    	alert.setHeaderText("You cannot choose the same Album");
			    	alert.setContentText("Choose a different Album");
			    	
			    	alert.showAndWait();
			    	copyPhotoAction(e);
			    	return;
			    }
			    newAlbum.addPhoto(photoList.get(photoBoxList.indexOf(selectedBox)));
			});
			
		}
		
	}
	/**
	 * Allows you to change the name of the current album
	 * @param e
	 */
	public void editAlbumAction(ActionEvent e) {
		TextInputDialog dialog = new TextInputDialog(album.getName());
		dialog.setTitle("Rename Album");
		dialog.setHeaderText("Create a new Album Name");
		dialog.setContentText("Please enter the Album Name:");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			for(Album a:user.getAlbums()) {
				if(a.getName().equals(result.get())){
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("Two Albums Cannot have the same name");
					alert.setContentText("Choose a new Album name");
					alert.showAndWait();
					editAlbumAction(e);
					return;
				}
			}
			album.setName(result.get());;
		}
		updateText();
	}
	public void updateText() {
		AlbumInfo.setText("Album Name: " + album.getName() + "\tNumber of Photos: " + album.getPhotos().size()
				+ "\tRange of dates: " + this.album.getLowDate() + "-" + this.album.getHighDate());
	
	}
	/**
	 * Closes the AlbumView scene and moves back to the UserSubSystem to view all of the users Albums
	 * @param e
	 */
	public void closeAlbumAction(ActionEvent e) {
		this.selectedBox = null;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/application/UserSubSystem.fxml"));
			Parent root = loader.load();
			
			UserSubSystemController ussc = loader.getController();
			ussc.start(mainStage,this.user);
			
			Scene scene = new Scene(root);
			mainStage.setScene(scene);
			mainStage.show();
		} catch (IOException ee) {
			// TODO Auto-generated catch block
			ee.printStackTrace();
		}
	}
	/**
	 * Updates currently displayed photos when a photo is deleted or addded
	 */
	public void updatePhotoView(){
		photoDisplay.getChildren().clear();
		for(VBox photo : photoBoxList) {
			if(photo.equals(selectedBox)) {
				photo.setStyle("-fx-padding: 0;" + 
	                      "-fx-border-style: solid inside;" + 
	                      "-fx-border-width: 2;" +
	                      "-fx-border-insets: 5;" + 
	                      "-fx-border-color: gray;");
			}else {
				photo.setStyle("");
			}
			photoDisplay.getChildren().add(photo);
		}
	}
	/**
	 * Creates a Vertical Box which houses the photo as an ImageView and the Images caption below that for displaying in the scene
	 * @param photo
	 * @return
	 */
	public VBox makePhotoView(Photo photo) {
		VBox view = new VBox();
		view.setAlignment(Pos.CENTER);
		
		FileInputStream input = null;
		Image image = null;
			try {
				input = new FileInputStream(photo.getAddress());
				image = new Image(input);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		ImageView imageView = new ImageView(image);
		imageView.setFitHeight(50);
		imageView.setFitWidth(50);

		Text title = new Text(photo.getCaption());
		
		view.getChildren().add(imageView);
		view.getChildren().add(title);
		view.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				//System.out.println(event);
				selectedBox = (VBox) event.getSource();
				updatePhotoView();
				event.consume();
			}
		});
		return view;
	}
 
	
}
