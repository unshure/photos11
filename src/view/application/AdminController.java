package view.application;

import java.util.ArrayList;

import application.Photos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class AdminController {
	
	@FXML
	private Button logout_button;
	
	@FXML private Button create;
	@FXML private Button delete;
	
	@FXML
	private ObservableList<String> usernames;
	
	@FXML
	private ListView<String> userlist;
	public Stage mainStage;
	
	/**
	 * Start method for Admin Controller
	 * @param mainStage Main Stage
	 */
	public void start(Stage mainStage) {
		this.mainStage = mainStage;
		
		create.setOnAction(this::createUserPrompt);
		delete.setOnAction(this::deleteUserPrompt);
		displayUser();
	}
	/**
	 * Starts a prompt to make a new user. Takes in Username and Password
	 * @param event
	 */
	public void createUserPrompt(ActionEvent event) {
		
		try {
            Stage stage = new Stage();
            Parent create_user_root = FXMLLoader.load(getClass().getResource("/view/application/Create_User.fxml"));
			stage.setScene(new Scene(create_user_root));  
			stage.showAndWait();
			displayUser();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Starts a prompt to delete a user. Takes in the username of the user to delete
	 * @param event
	 */
	public void deleteUserPrompt(ActionEvent event) {
		
		try {
            Stage stage = new Stage();
            Parent delete_user_root = FXMLLoader.load(getClass().getResource("/view/application/Delete_User.fxml"));
			stage.setScene(new Scene(delete_user_root));  
			stage.showAndWait();
			displayUser();
		} catch(Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * update function to view current users in user ListView
	 */
	public void displayUser() {
		ArrayList<String> usernameList = new ArrayList<String>();
		for(int i=0; i<Photos.Users.size(); i++) {
			usernameList.add(Photos.Users.get(i).getUsername());
		}
		
		usernames = FXCollections.observableArrayList(usernameList);
		userlist.setItems(usernames);
	}
	/**
	 * Logout of Admin account and brings back to login screen
	 * @param event
	 */
	public void logout(ActionEvent event) {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/application/Login.fxml"));
			Parent root = loader.load();
			
			LoginController lc = loader.getController();
			lc.start(mainStage);
			
			Scene scene = new Scene(root);
			this.mainStage.setScene(scene);
			this.mainStage.show();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}

}
