package view.application;

import java.util.ArrayList;

import application.Photos;
import fields.application.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */
public class Admin {
	
	@FXML
	private TextField created_username;
	
	@FXML
	private TextField created_password;
	
	@FXML
	private TextField deleted_username;
	
	@FXML
	private Button cancel_button;
	
	@FXML
	private Label createstatus;
	
	@FXML
	private Label deletestatus;
	
	/**
	 * Creates a new User
	 * @param event
	 */
	public void createUser(ActionEvent event) {
		
		ArrayList<User> userlist = Photos.Users;
		boolean userExists = false;
		for(int i=0; i<userlist.size(); i++) {
			if(userlist.get(i).getUsername().equals(created_username.getText()) || created_username.getText().equals("admin")) {
				userExists = true;
				break;
			}
		}
		
		if(userExists == false) {
			User new_user = new User();
			new_user.setUsername(created_username.getText());
			new_user.setPassword(created_username.getText());
			userlist.add(new_user);
			closeScene(event);
		}else {
			createstatus.setText("Username already exists");
		}
	}
	/**
	 * Deletes a current User
	 * @param event
	 */
	public void deleteUser(ActionEvent event) {
		
		ArrayList<User> userlist = Photos.Users;
		boolean userExists = false;
		int userindex = -1;
		for(int i=0; i<userlist.size(); i++) {
			if(userlist.get(i).getUsername().equals(deleted_username.getText())) {
				userExists = true;
				userindex = i;
				break;
			}
		}
		
		if(userExists == true) {
			userlist.remove(userindex);
			closeScene(event);
		}else {
			deletestatus.setText("Username does not exist");
		}
		
	}
	/**
	 * Closes Current Window
	 * @param event
	 */
	public void closeScene(ActionEvent event) {
        Stage prompt_window = (Stage) cancel_button.getScene().getWindow();
        prompt_window.close();
	}

}
