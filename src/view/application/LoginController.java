package view.application;

import java.util.ArrayList;

import application.Photos;
import fields.application.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 */

public class LoginController {
	
	@FXML
	private Label lblstatus;
	
	@FXML
	private TextField txtusername;
	
	@FXML
	private TextField txtpassword;
	
	Stage mainStage;
	/**
	 * Start method for Login controller
	 * @param mainStage
	 */
	public void start(Stage mainStage) {
		this.mainStage = mainStage;
	}
	
	/**
	 * Checks if the current Username and password match a current user or the admin in order for a user to log in
	 * @param event
	 */
	public void Login(ActionEvent event) {
		if(txtusername.getText().equals("admin") && txtpassword.getText().equals("admin")) {
			//If username and password are both admin, you login as admin and get redirected to admin page
			lblstatus.setText("Logged in as Admin");
			try {
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/application/Admin_Main.fxml"));
				Parent root = loader.load();
				
				AdminController ac = loader.getController();
				ac.start(mainStage);
								
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();

			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}else {
			ArrayList<User> users = Photos.Users;
			if(users == null) {
				lblstatus.setText("Login Failed");
			}else {
				boolean userExists = false;
				for(int i=0; i<users.size(); i++) {
					//Search through ArrayList of Users. If you find a username and password combo that matches
					//Congrats! You're logged in. Else you entered an invaled combo
					if(txtusername.getText().equals(users.get(i).getUsername()) && 
							txtpassword.getText().equals(users.get(i).getUsername())) {
						lblstatus.setText("Login Success");
						userExists = true;
						try {
							FXMLLoader loader = new FXMLLoader();
							loader.setLocation(getClass().getResource("/view/application/UserSubSystem.fxml"));
							Parent root = loader.load();
							
							UserSubSystemController abc = loader.getController();
							abc.start(mainStage,users.get(i));
							
							Scene scene = new Scene(root);
							this.mainStage.setScene(scene);
							this.mainStage.show();
						} catch(Exception ex) {
							ex.printStackTrace();
						}
						break;
					}
				}
				
				if(userExists == false) {
					lblstatus.setText("Login Failed");
				}
			}
		}
	}

}
